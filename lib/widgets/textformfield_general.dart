import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget textEditRegularRegister(
    TextEditingController textEditingController,
    String titulo,
    String hint,
    TextInputType type,
    bool isIconRight,
    Function function,
    bool isLock,
    bool withInteraction) {
  return Padding(
    padding: const EdgeInsets.only(top: 16.0, left: 41.0, right: 41.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            function();
          },
          child: Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black38,
                    blurRadius: 0,
                    offset: const Offset(0, 1),
                  ),
                ],
                border: Border.all(color: Color.fromRGBO(219, 219, 219, 1))),
            height: 50.0,
            child: TextFormField(
              onChanged: (value) {},
              enableInteractiveSelection:
                  withInteraction == true ? true : false,
              enabled: withInteraction == true ? true : false,
              validator: (value) {
                if (value!.isEmpty) {}
              },
              controller: textEditingController,
              keyboardType: type,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(
                    width: 0,
                    style: BorderStyle.none,
                  ),
                ),
                contentPadding: EdgeInsets.only(
                  top: 5.0,
                  left: 15,
                ),
                hintText: hint,
                // prefixIcon: Icon(
                //   Icons.lock,
                //   color: Color.fromRGBO(162, 162, 162, 1),
                // ),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
