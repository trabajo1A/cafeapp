import 'package:flutter/material.dart';

Widget buildButton(
  Function funtion,
  String titleButton,
  Color color,
  Color colorText,
) {
  return Container(
    padding: EdgeInsets.only(
      left: 51.0,
      right: 51.0,
      bottom: 5.93,
    ),
    width: double.infinity,
    // ignore: deprecated_member_use
    child: ElevatedButton(
      //disabledColor: Colors.grey,

      onPressed: () {
        funtion();
      },
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.all(8.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        backgroundColor: color,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            '$titleButton',
            style: TextStyle(
              color: colorText,
              fontSize: 15.0,
            ),
          ),
        ],
      ),
    ),
  );
}
