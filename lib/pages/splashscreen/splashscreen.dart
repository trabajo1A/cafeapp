import 'dart:convert';

import 'package:cafe_app/pages/home/home_page.dart';
import 'package:cafe_app/pages/login/login_page.dart';
import 'package:cafe_app/variables/variables.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  State<SplashScreenPage> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    Timer(Duration(milliseconds: 500), () => changeView());

    super.initState();
  }

  changeView() async {
    Timer(Duration(milliseconds: 1500), () async {
      if (prefs.idReferences != null && prefs.idReferences != "") {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => HomePage()));
      } else {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => LoginPage()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(194, 4, 48, 1),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(child: Image.asset("assets/cafe.png")),
        ],
      ),
    );
  }
}
