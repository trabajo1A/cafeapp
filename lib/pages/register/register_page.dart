import 'dart:developer';

import 'package:cafe_app/variables/variables.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../widgets/button_general.dart';
import '../../widgets/textformfield_general.dart';
import '../home/home_page.dart';

const List<String> list = <String>[
  'Americano',
  'Cappuccino',
  'Expresso',
  'Latte',
  'Flat white'
];

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController nombre = TextEditingController();
  TextEditingController apellido = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController pass = TextEditingController();
  TextEditingController passC = TextEditingController();
  String cucharadas = "";
  String cafetype = "";
  String dropdownValue = list.first;
  double _value = 0;

  Future signUp(String apellido, String azucar, String cafe, String correo,
      String nombre, String password) async {
    await FirebaseFirestore.instance
        .collection('users')
        .where('correo', isEqualTo: correo)
        .get()
        .then((snapshot) async {
      snapshot.docs;
      print(snapshot.docs);

      if (snapshot.docs.isEmpty) {
        log("se puede agregar");
        await FirebaseFirestore.instance.collection('users').add({
          'apellido': apellido,
          'azucar': azucar,
          'cafe': cafe,
          'correo': correo,
          'nombre': nombre,
          'password': password,
        }).whenComplete(() async {
          await FirebaseFirestore.instance
              .collection('users')
              .where('correo', isEqualTo: correo)
              .get()
              .then(
                (snapshot) => snapshot.docs.forEach(
                  (document) {
                    print(document.reference.id);
                    print("${document["nombre"]} ${document["apellido"]}");

                    prefs.idReferences = document.reference.id;
                    prefs.nombre = document["nombre"];
                    prefs.apellido = document["apellido"];
                    prefs.azucar = document["azucar"];
                    prefs.cafe = document["cafe"];
                    prefs.correo = document["correo"];

                    if (prefs.idReferences != null &&
                        prefs.idReferences != "") {
                      Fluttertoast.showToast(
                        msg: "Bienvenido",
                        timeInSecForIosWeb: 2,
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.TOP,
                        backgroundColor: Colors.green,
                        webShowClose: true,
                      );
                      Navigator.pop(context);
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (context) => HomePage()));
                    }
                  },
                ),
              );
        });
      } else {
        Fluttertoast.showToast(
          msg:
              "El correo usado ya se encuentra registrado, utiliza otro correo",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          webShowClose: true,
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
    );
  }

  Widget body() {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 60,
          ),
          SizedBox(
            height: 100,
            child: Image.asset("assets/coffe.jpeg"),
          ),
          Text(
            "Coffee and Office",
            style: TextStyle(
              fontFamily: 'coffe',
              fontSize: 40,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            "Registro",
            style: TextStyle(
              fontFamily: 'coffe',
              fontSize: 25,
              fontWeight: FontWeight.normal,
            ),
          ),
          textEditRegularRegister(nombre, "Nombre", "Nombre",
              TextInputType.text, false, () {}, false, true),
          textEditRegularRegister(apellido, "Apellido", "Apellido",
              TextInputType.text, false, () {}, false, true),
          textEditRegularRegister(email, "Correo electronico", "Correo",
              TextInputType.emailAddress, false, () {}, false, true),
          textEditRegularRegister(pass, "password", "Contraseña",
              TextInputType.visiblePassword, false, () {}, false, true),
          textEditRegularRegister(passC, "password", "Contraseña",
              TextInputType.visiblePassword, false, () {}, false, true),
          SizedBox(
            height: 20,
          ),
          componentCoffe(),
          componentAzucar(),
          SizedBox(
            height: 40,
          ),
          buildButton(
            () {
              signUp(
                  apellido.text.trim(),
                  cucharadas.toString(),
                  cafetype.toString(),
                  email.text.trim(),
                  nombre.text.trim(),
                  pass.text.trim());
            },
            "Registrar",
            Colors.green,
            Colors.white,
          ),
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Text("Ya estoy registrado"),
          ),
          SizedBox(
            height: 40,
          ),
        ],
      ),
    );
  }

  Widget componentAzucar() {
    return Column(
      children: [
        SizedBox(
          height: 30,
        ),
        Text("¿Cuantas cucharadas de azucar?: "),
        Slider(
          min: 0.0,
          max: 6.0,
          divisions: 6,
          label: '${_value.round()} cucharadas',
          value: _value,
          onChanged: (value) {
            setState(() {
              print(value);
              cucharadas = value.toString();
              _value = value;
            });
          },
        ),
      ],
    );
  }

  Widget componentCoffe() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("¿Que cafe quieres?: "),
        DropdownButton<String>(
          value: dropdownValue,
          icon: Icon(Icons.arrow_drop_down),
          elevation: 5,
          items: list.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
          onChanged: (String? value) {
            // This is called when the user selects an item.
            setState(() {
              log(value!);
              cafetype = value;
              dropdownValue = value;
            });
          },
        ),
      ],
    );
  }
}
