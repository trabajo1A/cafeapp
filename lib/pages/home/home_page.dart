import 'package:cafe_app/pages/login/login_page.dart';
import 'package:cafe_app/pages/settings/settings_page.dart';
import 'package:cafe_app/readData/getUserName.dart';
import 'package:cafe_app/variables/variables.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //final user = FirebaseAuth.instance.currentUser!;

  //documents IDs
  List<String> docIDs = [];

  //get doc
  Future getDocId() async {
    await FirebaseFirestore.instance.collection('users').get().then(
          (snapshot) => snapshot.docs.forEach(
            (document) {
              print(document.reference);
              docIDs.add(document.reference.id);
            },
          ),
        );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //getDocId();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cafes Apps"),
        backgroundColor: Colors.brown[400],
        actions: [
          GestureDetector(
            onTap: () {
              //cerrar sesión
              docIDs = [];
              prefs.prefs!.clear();
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => LoginPage()));
            },
            child: Row(
              children: [
                Icon(Icons.person),
                Text("LogOut"),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 8),
            child: VerticalDivider(
              color: Colors.white,
              thickness: 2,
            ),
          ),
          GestureDetector(
            onTap: () {
              //configuracion de nombre y del cafe
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SettingsPage(),
                ),
              );
            },
            child: Row(
              children: [
                Icon(Icons.settings),
                Text("Settings"),
              ],
            ),
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
      body: body(),
    );
  }

  Widget body() {
    return Center(
      child: Column(
        children: [
          Expanded(
              child: FutureBuilder(
                  future: getDocId(),
                  builder: (context, snapshot) {
                    return ListView.builder(
                      itemCount: docIDs.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: GetUserName(documentsId: docIDs[index]),
                        );
                      },
                    );
                  })),
        ],
      ),
    );
  }
}
