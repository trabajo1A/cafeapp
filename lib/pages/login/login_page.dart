import 'dart:ui';

import 'package:cafe_app/pages/home/home_page.dart';
import 'package:cafe_app/pages/register/register_page.dart';
import 'package:cafe_app/widgets/button_general.dart';
import 'package:cafe_app/widgets/textformfield_general.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../variables/variables.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController email = TextEditingController();
  TextEditingController pass = TextEditingController();

  Future signIn(String correo, String password) async {
    await FirebaseFirestore.instance
        .collection('users')
        .where('correo', isEqualTo: correo)
        .where('password', isEqualTo: password)
        .get()
        .then((snapshot) {
      print(snapshot.docs);
      if (snapshot.docs.isNotEmpty) {
        snapshot.docs.forEach((document) {
          print(document["nombre"]);
          prefs.idReferences = document.reference.id;
          prefs.nombre = document["nombre"];
          prefs.apellido = document["apellido"];
          prefs.azucar = document["azucar"];
          prefs.cafe = document["cafe"];
          prefs.correo = document["correo"];
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomePage()));
        });
      } else {
        Fluttertoast.showToast(
          msg: "El correo o la contraseña es erronea",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          webShowClose: true,
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
    );
  }

  Widget body() {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 40,
          ),
          SizedBox(
            height: 300,
            child: Image.asset("assets/coffe.jpeg"),
          ),
          Text(
            "Coffee and Office",
            style: TextStyle(
              fontFamily: 'coffe',
              fontSize: 40,
              fontWeight: FontWeight.bold,
            ),
          ),
          textEditRegularRegister(email, "Correo electronico", "Correo",
              TextInputType.emailAddress, false, () {}, false, true),
          textEditRegularRegister(pass, "password", "Contraseña",
              TextInputType.visiblePassword, false, () {}, false, true),
          SizedBox(
            height: 40,
          ),
          buildButton(
            () async {
              signIn(email.text.trim(), pass.text.trim());
            },
            "Iniciar sesion",
            Colors.green,
            Colors.white,
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => RegisterPage(),
                ),
              );
            },
            child: Text("Registrarse"),
          ),
        ],
      ),
    );
  }
}
