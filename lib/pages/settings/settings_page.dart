import 'dart:developer';

import 'package:cafe_app/variables/variables.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../widgets/button_general.dart';
import '../../widgets/textformfield_general.dart';
import '../register/register_page.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  TextEditingController nombre = TextEditingController();
  TextEditingController apellido = TextEditingController();
  String cucharadas = "";
  String cafetype = "";
  String dropdownValue = list.first;
  double _value = 0;

  Future updateUser(
      String nombre2, String apellido2, String azucar, String cafe) async {
    await FirebaseFirestore.instance
        .collection('users')
        .doc(prefs.idReferences)
        .update(
      {
        'apellido': apellido2 == "" ? prefs.apellido : apellido2,
        'nombre': nombre2 == "" ? prefs.nombre : nombre2,
        //'correo': prefs.correo,
        'azucar': azucar == "" ? prefs.azucar : azucar,
        'cafe': cafe == "" ? prefs.cafe : azucar,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        title: Text("Mi Perfil"),
        centerTitle: true,
      ),
      body: body(),
    );
  }

  Widget body() {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 15,
          ),
          textEditRegularRegister(nombre, "Nombre", prefs.nombre,
              TextInputType.text, false, () {}, false, true),
          textEditRegularRegister(apellido, "Apellido", prefs.apellido,
              TextInputType.text, false, () {}, false, true),
          SizedBox(
            height: 20,
          ),
          componentCoffe(),
          componentAzucar(),
          SizedBox(
            height: 30,
          ),
          buildButton(
            () {
              updateUser(nombre.text.trim(), apellido.text.trim(),
                  cucharadas.toString(), cafetype.toString());
              Navigator.pop(context);
            },
            "Actualizar",
            Colors.green,
            Colors.white,
          ),
        ],
      ),
    );
  }

  Widget componentAzucar() {
    return Column(
      children: [
        SizedBox(
          height: 30,
        ),
        Text("¿Cuantas cucharadas de azucar?: "),
        Slider(
          min: 0.0,
          max: 6.0,
          divisions: 6,
          label: '${_value.round()} cucharadas',
          value: _value,
          onChanged: (value) {
            setState(() {
              print(value);
              cucharadas = value.toString();
              _value = value;
            });
          },
        ),
      ],
    );
  }

  Widget componentCoffe() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("¿Que cafe quieres?: "),
        DropdownButton<String>(
          value: dropdownValue,
          icon: Icon(Icons.arrow_drop_down),
          elevation: 5,
          items: list.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
          onChanged: (String? value) {
            // This is called when the user selects an item.
            setState(() {
              log(value!);
              cafetype = value;
              dropdownValue = value;
            });
          },
        ),
      ],
    );
  }
}
