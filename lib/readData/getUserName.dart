import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class GetUserName extends StatelessWidget {
  final String documentsId;

  GetUserName({required this.documentsId});

  @override
  Widget build(BuildContext context) {
    CollectionReference users = FirebaseFirestore.instance.collection('users');
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(documentsId).get(),
      builder: ((context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
          return Column(
            children: [
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                elevation: 5,
                child: Container(
                  height: MediaQuery.of(context).size.height * .249,
                  width: MediaQuery.of(context).size.width * 0.95,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              "Nombre",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Text("${data["nombre"]} ${data["apellido"]}"),
                        Text("Cantidad de azucar: ${data["azucar"]}"),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Tipo de cafe: ${data["cafe"]}"),
                            data["cafe"] == "Americano"
                                ? SizedBox(
                                    height: 80,
                                    child: Image.asset("assets/descarga.jpeg"),
                                  )
                                : data["cafe"] == "Cappuccino"
                                    ? SizedBox(
                                        height: 80,
                                        child: Image.asset(
                                            "assets/capuchino.jpeg"),
                                      )
                                    : data["cafe"] == "Expresso"
                                        ? SizedBox(
                                            height: 80,
                                            child: Image.asset(
                                                "assets/expreso.jpeg"),
                                          )
                                        : data["cafe"] == "Latte"
                                            ? SizedBox(
                                                height: 80,
                                                child: Image.asset(
                                                    "assets/latte.jpeg"),
                                              )
                                            : data["cafe"] == "Flat white"
                                                ? SizedBox(
                                                    height: 80,
                                                    child: Image.asset(
                                                        "assets/flat-white.jpeg"),
                                                  )
                                                : SizedBox(
                                                    height: 80,
                                                    child: Image.asset(
                                                        "assets/descarga.jpeg"),
                                                  )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
        }
        return Text("Cargando ......");
      }),
    );
  }
}
