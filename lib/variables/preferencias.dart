import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences? prefs;

  initPrefs() async {
    WidgetsFlutterBinding.ensureInitialized();
    this.prefs = await SharedPreferences.getInstance();
  }

  String get idReferences {
    return this.prefs!.getString("idReferences") ?? "";
  }

  set idReferences(String idReferences) {
    this.prefs!.setString("idReferences", idReferences);
  }

  String get nombre {
    return this.prefs!.getString("nombre") ?? "";
  }

  set nombre(String nombre) {
    this.prefs!.setString("nombre", nombre);
  }

  String get apellido {
    return this.prefs!.getString("apellido") ?? "";
  }

  set apellido(String apellido) {
    this.prefs!.setString("apellido", apellido);
  }

  String get azucar {
    return this.prefs!.getString("azucar") ?? "";
  }

  set azucar(String azucar) {
    this.prefs!.setString("azucar", azucar);
  }

  String get cafe {
    return this.prefs!.getString("cafe") ?? "";
  }

  set cafe(String cafe) {
    this.prefs!.setString("cafe", cafe);
  }

  String get correo {
    return this.prefs!.getString("correo") ?? "";
  }

  set correo(String correo) {
    this.prefs!.setString("correo", correo);
  }
}
